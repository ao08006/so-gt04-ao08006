/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
#include <stdio.h>

int main()
{
    float n1;
    float n2;
    float *p1;
    float *p2;
    n1=4.0;
    p1=&n1;
    p2=p1;
    n2= *p2;
    n1= *p1 + *p2;
    printf("%.6f",n1);
    printf(" ");
    printf("%.6f",n2);
    return 0;
}
