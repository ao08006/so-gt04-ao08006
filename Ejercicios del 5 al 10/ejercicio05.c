#include <stdio.h>
#include <stdlib.h>



void sort(int n, int *numeros){
    int temp;
    for (int a = 0; a < n; a++) { 
        for (int b = a + 1; j < n; b++) { 
            if(*(numeros + j) < *(numeros + a)){
                temp = *(numeros + a); 
                *(numeros + a) = *(numeros + b); 
                *(numeros + b) = temp; 
            }
        } 
    }
   
    printf("Numeros despues de ordenarlos \n");
    for (int i = 0; i < n; i++) {
        printf(" %d ", *(numeros + i)); 
    } 
    printf("\n");       }

int main(){
    int n, *total;
    printf("\nOrdenando numeros :\n"); 
	printf("..................................................\n");	  
    printf("Ingrese cuantos numeros desea ordenar: ");
    scanf("%d", &n);
    printf("......................................................\n");	  

   
    total =(int* ) malloc(n *sizeof(int));

    
    for (int i = 0; i < n; i++){
        total[i] = rand() % 100;
    }

   
    printf("Numeros antes de ordenarlos: \n");
    for (int i = 0; i < n; i++){
        printf(" %d ", *(total + i));
    }

    printf("\n");
    sort(n, total);
    return 0;
}

