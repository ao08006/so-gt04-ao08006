#include <stdio.h>
#include <stdlib.h>

void operaciones(int n, float *datos){
    float mayor = 0.00, menor = 100.00, media=0.00;
    for (int a = 0; a < n; a++) { 
        if(datos[a]>mayor){
            mayor=datos[a];
        }
        if(menor>datos[a]){
            menor=datos[a];
        }
        media=media+datos[a];
    }
    
    media=media/n;
    printf("\nDato mayor : %.2f",mayor);
    printf("\nDato menor : %.2f",menor);
    printf("\nMedia Aritmetica : %.2f",media);
    printf("\n");      
}
int main(){
    int n;
    float ingreso,*datos;
    printf("Ingrese cantidad de datos a operar: ");
    scanf("%d", &n);

   
    datos =(float* ) malloc(n *sizeof(int));

    
    for (int i = 0; i < n; i++){
        printf("Ingrese dato #%d",i+1);
        printf("\t");
        scanf("%f", &ingreso);
        datos[i] = ingreso;
    }

   
    printf("Datos ingresados: \n");
    for (int i = 0; i < n; i++){
        printf(" %.2f", *(datos + i));
    }

    printf("\n");

    operaciones(n,datos);
    return 0;

}